import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class RiverpodScreen extends ConsumerWidget {
  @override
  Widget build(BuildContext context, ScopedReader watch) {
    final count = watch<CounterNotifier>(counterNotifierProvider).count;
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          watch<CounterNotifier>(counterNotifierProvider).up();
        },
        child: Icon(Icons.add),
      ),
      body: Text('$count'),
    );
  }
}

final counterNotifierProvider = ChangeNotifierProvider<CounterNotifier>((_) {
  return CounterNotifier();
});

class CounterNotifier extends ChangeNotifier {
  int _count = 0;
  int get count => _count;
  void up() {
    _count++;
    notifyListeners();
  }
}
