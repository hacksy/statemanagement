import 'package:flutter/material.dart';

class InheritedWidgetScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return CounterWidget(
      child: Scaffold(
        floatingActionButton: Builder(
          builder: (context) {
            return FloatingActionButton(
              onPressed: () {
                CounterWidget.of(context).update();
              },
              child: Icon(Icons.add),
            );
          },
        ),
        body: Center(
          child: Builder(
            builder: (context) {
              return Text('${CounterWidget.of(context).value}');
            },
          ),
        ),
      ),
    );
  }
}

class CounterInheritedWidget extends InheritedWidget {
  final CounterWidgetState data;
  final Widget child;

  CounterInheritedWidget({
    Key key,
    @required this.data,
    @required this.child,
  }) : super(key: key, child: child);

  @override
  bool updateShouldNotify(covariant CounterInheritedWidget oldWidget) {
    return true;
  }
}

class CounterWidget extends StatefulWidget {
  CounterWidget({this.child});
  final Widget child;

  static CounterWidgetState of(BuildContext context) {
    return context
        .dependOnInheritedWidgetOfExactType<CounterInheritedWidget>()
        .data;
  }

  @override
  State<CounterWidget> createState() {
    return CounterWidgetState();
  }
}

class CounterWidgetState extends State<CounterWidget> {
  int _data = 0;
  int get value => _data;
  void update() {
    setState(() {
      _data++;
    });
  }

  @override
  Widget build(BuildContext context) {
    return CounterInheritedWidget(
      data: this,
      child: widget.child,
    );
  }
}
