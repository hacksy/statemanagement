import 'package:flutter/material.dart';

class SetStateScreen extends StatefulWidget {
  @override
  _SetStateScreenState createState() => _SetStateScreenState();
}

class _SetStateScreenState extends State<SetStateScreen> {
  int _value = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: _onPressed,
      ),
      body: Center(
        child: Text('$_value'),
      ),
    );
  }

  void _onPressed() async {
    setState(updateState);
    Future.delayed(Duration(microseconds: 27)).then((value) {
      _value++;
    });
  }

  void updateState() {}
}
