import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ProviderScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<ValueProvider>(
      create: (context) {
        return ValueProvider();
      },
      child: Scaffold(
        floatingActionButton: Builder(
          builder: (BuildContext context) {
            return FloatingActionButton(
              onPressed: () {
                Provider.of<ValueProvider>(context, listen: false).update();
                //context.read<ValueProvider>().update();
              },
              child: Icon(Icons.add),
            );
          },
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Consumer<ValueProvider>(
                builder: (BuildContext context, ValueProvider changeNotifier,
                    Widget child) {
                  return Text('${changeNotifier.value}');
                },
              ),
              Builder(
                builder: (context) {
                  //  final int =
                  //     Provider.of<ValueProvider>(context, listen: false).value;
                  final int = context.watch<ValueProvider>().value;
                  return Text('$int');
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class ValueProvider extends ChangeNotifier {
  int _value = 0;
  int get value => _value;

  void update() {
    _value++;
    notifyListeners();
  }
}
