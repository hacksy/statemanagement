import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';

class RxDartScreen extends StatefulWidget {
  @override
  _RxDartScreenState createState() => _RxDartScreenState();
}

class _RxDartScreenState extends State<RxDartScreen> {
  CounterBloc counterBloc = CounterBloc(initialValue: 0);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          counterBloc.up();
        },
        child: Icon(Icons.add),
      ),
      body: Center(
        child: StreamBuilder<int>(
          initialData: -1,
          stream: counterBloc.counterStream,
          builder: (context, snapshot) {
            if (snapshot.hasError) {
              return Text("Error");
            }
            if (snapshot.hasData) {
              return Text('${snapshot.data}');
            }
            return CircularProgressIndicator();
          },
        ),
      ),
    );
  }
}

class CounterBloc {
  int initialValue = 0;

  BehaviorSubject<int> _subjectCounter;
  Stream get counterStream => _subjectCounter.stream;
  CounterBloc({this.initialValue}) {
    _subjectCounter = BehaviorSubject<int>.seeded(this.initialValue);
  }

  void up() {
    initialValue++;
    _subjectCounter.sink.add(initialValue);
  }

  void dispose() {
    _subjectCounter.close();
  }
}
