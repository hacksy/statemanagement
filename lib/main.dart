import 'package:flutter/material.dart';
import 'package:state_management_tool/inherited_widget_screen.dart';
import 'package:state_management_tool/provider_screen.dart';
import 'package:state_management_tool/riverpod_screen.dart';
import 'package:state_management_tool/rxdart_screen.dart';
import 'package:state_management_tool/scope_model_screen.dart';
import 'package:state_management_tool/set_state_screen.dart';

import 'bloc_screen.dart';
import 'getx_screen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      // showPerformanceOverlay: true,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: StateManagementPage(),
    );
  }
}

class StateManagementPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('State Management '),
      ),
      body: Container(
        child: Column(
          children: [
            RaisedButton(
              onPressed: () {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (context) {
                  return SetStateScreen();
                }));
              },
              child: Text("SetState"),
            ),
            RaisedButton(
              onPressed: () {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (context) {
                  return ScopedModelScreen();
                }));
              },
              child: Text("Scope Model"),
            ),
            RaisedButton(
              onPressed: () {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (context) {
                  return InheritedWidgetScreen();
                }));
              },
              child: Text("Inherited Widget"),
            ),
            RaisedButton(
              onPressed: () {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (context) {
                  return ProviderScreen();
                }));
              },
              child: Text("Provider"),
            ),
            RaisedButton(
              onPressed: () {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (context) {
                  return RxDartScreen();
                }));
              },
              child: Text("RxDart"),
            ),
            RaisedButton(
              onPressed: () {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (context) {
                  return GetXScreen();
                }));
              },
              child: Text("GetX"),
            ),
            RaisedButton(
              onPressed: () {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (context) {
                  return BlocScreen();
                }));
              },
              child: Text("Bloc Package"),
            ),
            RaisedButton(
              onPressed: () {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (context) {
                  return RiverpodScreen();
                }));
              },
              child: Text("Riverpod"),
            ),
          ],
        ),
      ),
    );
  }
}
