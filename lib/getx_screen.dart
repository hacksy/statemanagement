import 'package:flutter/material.dart';
import 'package:get/get.dart';

class GetXScreen extends StatelessWidget {
  final GetCounterController controller = Get.put(GetCounterController());
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Obx(() {
          return Text('${controller.counter.value}');
        }),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          controller.up();
        },
        child: Icon(Icons.add),
      ),
    );
  }
}

class GetCounterController extends GetxController {
  final counter = 0.obs;
  void up() {
    counter.value++;
  }
}
