import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class BlocScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider<CounterCubit>(
      create: (context) => CounterCubit(),
      child: Scaffold(
        floatingActionButton: Builder(
          builder: (context) {
            return FloatingActionButton(
              onPressed: () {
                //BlocProvider.of<CounterCubit>(context, listen: false).up();
                context.read<CounterCubit>().up();
              },
              child: Icon(Icons.add),
            );
          },
        ),
        body: Center(
          child: Builder(
            builder: (context) {
              final value = context.watch<CounterCubit>().state;
              return Text('$value');
            },
          ),
        ),
      ),
    );
  }
}

class CounterCubit extends Cubit<int> {
  CounterCubit() : super(0);
  void up() {
    emit(state + 1);
  }
}
