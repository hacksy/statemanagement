import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

class ScopedModelScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ScopedModel<ValueModel>(
      model: ValueModel(),
      child: Scaffold(
        floatingActionButton: Builder(
          builder: (context) {
            return FloatingActionButton(
              onPressed: () {
                ScopedModel.of<ValueModel>(context).up();
              },
              child: Icon(Icons.add),
            );
          },
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              ScopedModelDescendant<ValueModel>(
                builder:
                    (BuildContext context, Widget child, ValueModel model) {
                  return Text('Forma 1: ${model.value}');
                },
              ),
              Builder(
                builder: (context) {
                  final _value =
                      ScopedModel.of<ValueModel>(context, rebuildOnChange: true)
                          .value;
                  return Text('Forma 2: $_value');
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class ValueModel extends Model {
  int _value = 0;

  int get value => _value;

  void up() {
    _value++;
    notifyListeners();
  }
}
